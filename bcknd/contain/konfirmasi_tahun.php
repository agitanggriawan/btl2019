<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Konfirmasi Tahun</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Edit Konfirmasi Tahun</strong>
                        </div>
                    </div>
                    <?php 
                        // Jika Sukses
                        /*<div class="alert  alert-success alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-success">Success</span> You successfully read this important alert message.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>*/
                        if(isset($_POST['edit']))
                        {
                            $id_tahun = $_POST['tahun'];

                            $sql_update_clear = $db->exec("UPDATE tahun
                                                        SET status_tahun = 'Tidak Aktif' ");

                            $sql_update = $db->exec("UPDATE tahun
                                                        SET status_tahun= 'Aktif'
                                                        WHERE id_tahun = ".$id_tahun." ");

                            if ($sql_update) {
                    ?>
                                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-success">Sukses</span> Data Sukses Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            } else {
                    ?>
                                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-danger">Gagal</span> Data Gagal Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            }
                        }
                    ?>
                    <?php
                        foreach($db->query('SELECT *
                                                FROM tahun
                                                WHERE id_tahun = '.$_GET["id"].'') as $row) {
                    ?>
                    <div class="card-body card-block">
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-2">
                                </div>
                                <div class="col-12 col-md-6">
                                    <input type="text" id="text-input" name="tahun" value="<?php echo $row['id_tahun'] ?>" class="form-control" hidden>
                                </div>
                            </div>
                            <div align="center">
                                <h2 style="color:red;"><b>PERHATIAN..!!!</b></h2>
                                <h2>Tahun berubah menjadi <?php echo $row['nama_tahun'] ?><br/>Semua data yang tersimpan mengikuti tahun yang berubah</h2>
                            </div>
                            <br/>
                            <div align="center">
                                <button type="submit" class="btn btn-primary" name="edit">
                                    <i class="fa fa-check"></i> Ya, Saya Mengerti
                                </button>
                                <a class="btn btn-outline-secondary" href="index.php?contain=master_tahun" role="button"><i class="fa fa-mail-reply"></i>&nbsp; Kembali</a>
                            </div>
                        </form>
                    </div>
                    <?php 
                        }
                    ?>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->