<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Laporan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<?php
    include "../include/connect.php";

    if (isset($_POST['cari'])) {
        $stmt = $db->prepare("SELECT nama_skpd AS nama FROM skpd WHERE id_skpd = '".$_POST['skpd']."' ");
        $stmt -> execute();
        $invNum = $stmt -> fetch(PDO::FETCH_ASSOC);
        $nama = $invNum['nama'];
    }

    function NamaBulan($num) {
        $NamaBulan [1] = 'Januari';
        $NamaBulan [2] = 'Februari';
        $NamaBulan [3] = 'Maret';
        $NamaBulan [4] = 'April';
        $NamaBulan [5] = 'Mei';
        $NamaBulan [6] = 'Juni';
        $NamaBulan [7] = 'Juli';
        $NamaBulan [8] = 'Agustus';
        $NamaBulan [9] = 'September';
        $NamaBulan [10] = 'Oktober';
        $NamaBulan [11] = 'November';
        $NamaBulan [12] = 'Desember';
        $NamaBulan [13] = 'Gaji 13';
        $NamaBulan [14] = 'Gaji 14';

        return $NamaBulan[$num];
    }

    function NamaRealisasi($num) {
        $NamaRealisasi [1] = 'realisasi_januari';
        $NamaRealisasi [2] = 'realisasi_februari';
        $NamaRealisasi [3] = 'realisasi_maret';
        $NamaRealisasi [4] = 'realisasi_april';
        $NamaRealisasi [5] = 'realisasi_mei';
        $NamaRealisasi [6] = 'realisasi_juni';
        $NamaRealisasi [7] = 'realisasi_juli';
        $NamaRealisasi [8] = 'realisasi_agustus';
        $NamaRealisasi [9] = 'realisasi_september';
        $NamaRealisasi [10] = 'realisasi_oktober';
        $NamaRealisasi [11] = 'realisasi_november';
        $NamaRealisasi [12] = 'realisasi_desember';
        $NamaRealisasi [13] = 'realisasi_13';
        $NamaRealisasi [14] = 'realisasi_14';

        return $NamaRealisasi[$num];
    }

    function NamaAbjad($num) {
        $NamaAbjad [1] = 'D';
        $NamaAbjad [2] = 'E';
        $NamaAbjad [3] = 'F';
        $NamaAbjad [4] = 'G';
        $NamaAbjad [5] = 'H';
        $NamaAbjad [6] = 'I';
        $NamaAbjad [7] = 'J';
        $NamaAbjad [8] = 'K';
        $NamaAbjad [9] = 'L';
        $NamaAbjad [10] = 'M';
        $NamaAbjad [11] = 'N';
        $NamaAbjad [12] = 'O';
        $NamaAbjad [13] = 'P';
        $NamaAbjad [14] = 'Q';
        $NamaAbjad [15] = 'R';
        $NamaAbjad [16] = 'S';
        $NamaAbjad [17] = 'T';
        $NamaAbjad [18] = 'U';
        $NamaAbjad [19] = 'V';
        $NamaAbjad [20] = 'W';
        $NamaAbjad [21] = 'X';
        $NamaAbjad [22] = 'Y';

        return $NamaAbjad[$num];
    }
?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Laporan Belanja Tidak Langsung</strong>
                        </div>
                    </div>
                    <div class="card-body card-block" style="overflow-x:scroll;">
                        <form action="" method="POST">
                            <div class="row form-group" align="right">
                                <div class="col-2 col-md-2">
                                    <label for="text-input" class="form-control-label">SKPD</label>
                                </div>
                                <div class="col-10 col-md-6">
                                    <select class="form-control" name="skpd" required>
                                <?php
                                    if (isset($_POST['cari'])) {
                                ?>
                                        <option selected value="<?php echo $_POST['skpd']; ?>"><?php echo $nama; ?></option>';
                                <?php
                                    } else {
                                        echo '<option disabled selected>- Nama SKPD -</option>';
                                    }
                                ?>
                                    <?php
                                        foreach($db->query("SELECT * FROM skpd ORDER BY nama_skpd ASC") as $row) {
                                    ?>
                                        <option value="<?php echo $row['id_skpd']; ?>"><?php echo $row['nama_skpd']; ?></option>
                                    <?php 
                                        }
                                    ?>
                                    </select>

                                </div>
                            </div>
                            <div class="row form-group" align="right">
                                <div class="col-2 col-md-2">
                                    <label for="text-input" class="form-control-label">Bulan</label>
                                </div>
                                <div class="col-10 col-md-6">
                                    <select class="form-control" name="bulan" required>
                                    <?php
                                        if (isset($_POST['cari'])) {
                                    ?>
                                            <option selected value="<?php echo $_POST['bulan']; ?>"><?php echo NamaBulan($_POST['bulan']); ?></option>
                                    <?php
                                        } else {
                                            echo '<option disabled selected>- Bulan -</option>';
                                        }
                                    ?>
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                </div>
                                <div class="col-12 col-md-6" align="right">
                                    <button type="submit" class="btn btn-primary btn-sm" name="cari">
                                        <i class="fa fa-search"></i> Cari
                                    </button>
                                </div>
                            </div>        
                        </form>

                        <?php
                            if (isset($_POST['cari'])) {

                                // TAMBAHAN KOLOM BERDASARKAN GAJI 13/14
                                // TAMBAHAN KOLOM BERDASARKAN GAJI 13/14
                                foreach($db->query("SELECT status_gaji_13, status_gaji_14 FROM btl JOIN tahun ON btl.id_tahun = tahun.id_tahun
                                                    WHERE tahun.status_tahun = 'Aktif'") as $row) {
                                    if ($row['status_gaji_14'] == 'Aktif') {
                                        $last_kolom = $_POST['bulan']+2;
                                        $gaji = 14;
                                    } else if ($row['status_gaji_13'] == 'Aktif') {
                                        $last_kolom = $_POST['bulan']+1;
                                        $gaji = 13;
                                    } else {$last_kolom = $_POST['bulan'];}
                                }
                                // END TAMBAHAN KOLOM BERDASARKAN GAJI 13/14
                                // END TAMBAHAN KOLOM BERDASARKAN GAJI 13/14
                        ?>
                                <br>
                                <strong class="card-title">
                                    <h3 align="center">Laporan Belanja Tidak Langsung<br/><?php echo $nama; ?></h3><br/>
                                </strong>

                                <table id="bootstrap-data-table-export" class="table table-striped table-hover table-bordered">

                                    <thead>
                                        <tr align="center">
                                            <th rowspan="2" valign="middle">No.</th>
                                            <th rowspan="2" valign="middle">Keterangan</th>
                                            <th rowspan="2" valign="middle">Anggaran</th>
                                            <th colspan="<?php echo $last_kolom;?>">Realisasi</th>
                                            <th rowspan="2">Jumlah Realisasi</th>
                                            <th rowspan="2" valign="middle">Prediksi</th>
                                            <th rowspan="2" valign="middle">Kenaikan Gaji</th>
                                            <th rowspan="2" valign="middle">Total</th>
                                            <th rowspan="2" valign="middle">Selisih</th>
                                        </tr>
                                        <tr align="center">
                                        <?php
                                            for ($i=1; $i <= $_POST['bulan']; $i++) { 
                                                echo "<th>".NamaBulan($i)."</th>";
                                            }
                                            
                                            if ($gaji == 14) {
                                                echo "<th>".NamaBulan(13)."</th>";
                                                echo "<th>".NamaBulan(14)."</th>";
                                            } else if ($gaji == 13) {
                                                echo "<th>".NamaBulan(13)."</th>";
                                            }
                                        ?>
                                        </tr>
                                        <tr align="center" style="font-size:12px;">
                                            <th>A</th>
                                            <th>B</th>
                                            <th>C</th>
                                        <?php
                                            $abjadjumlah = "";
                                            for ($i=1; $i <= $last_kolom; $i++) { 
                                                echo "<th>".NamaAbjad($i)."</th>";
                                                if ($i != $last_kolom) {
                                                    $abjadjumlah .= NamaAbjad($i)."+";
                                                } else {
                                                    $abjadjumlah .= NamaAbjad($i);
                                                }
                                            }
                                            //Jumlah
                                            //Jumlah
                                            echo "<th>".NamaAbjad($last_kolom+1)."=".$abjadjumlah."</th>";
                                            //Prediksi
                                            //Prediksi
                                            echo "<th>".NamaAbjad($last_kolom+2)."=".NamaAbjad($_POST['bulan'])."x".(14-$last_kolom)."</th>";
                                            //Kenaikan Gaji
                                            //Kenaikan Gaji
                                            echo "<th>".NamaAbjad($last_kolom+3)."= 5%x".NamaAbjad($_POST['bulan'])."x14</th>";
                                            //Total
                                            //Total
                                            echo "<th>".NamaAbjad($last_kolom+4)."=".NamaAbjad($last_kolom+1)."+".NamaAbjad($last_kolom+2)."+".NamaAbjad($last_kolom+3)."</th>";
                                            //Selisih
                                            //Selisih
                                            echo "<th>".NamaAbjad($last_kolom+5)."=C-".NamaAbjad($last_kolom+4)."</th>";
                                        ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach($db->query("SELECT * FROM btl
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                JOIN skpd ON skpd.id_skpd = btl.id_skpd
                                                                WHERE tahun.status_tahun = 'Aktif' AND
                                                                        skpd.nama_skpd = '".$nama."' ") as $row) {
                                    ?>
                                        <tr>
                                            <td align="center"><?php echo $row['id_rincian']; ?></td>
                                            <td align="left"><?php echo $row['nama_rincian']; ?></td>
                                            <td align="right"><?php echo number_format($row['anggaran']); ?></td>

                                        <?php
                                            $jumlahrealisasi = 0;
                                            for ($i=1; $i <= $_POST['bulan']; $i++) {
                                                // Menampilkan Nilai Realisasi
                                                echo "<td align='right'>".number_format($row[NamaRealisasi($i)])."</td>";
                                                if ($i != $_POST['bulan']) {
                                                    $jumlahrealisasi += $row[NamaRealisasi($i)];
                                                } else {
                                                    $jumlahrealisasi += $row[NamaRealisasi($i)];
                                                }
                                            }

                                            // Menampilakn Nilai Gaji 13/14 jika ada
                                            // Menampilakn Nilai Gaji 13/14 jika ada
                                            if ($gaji == 14) {
                                                echo "<td align='right'>".number_format($row[NamaRealisasi(13)])."</td>";
                                                echo "<td align='right'>".number_format($row[NamaRealisasi(14)])."</td>";
                                                $jumlahrealisasi += $row[NamaRealisasi(13)];
                                                $jumlahrealisasi += $row[NamaRealisasi(14)];
                                            } else if ($gaji == 13) {
                                                echo "<td align='right'>".number_format($row[NamaRealisasi(13)])."</td>";
                                                $jumlahrealisasi += $row[NamaRealisasi(13)];
                                            }
                                            // End Menampilakn Nilai Gaji 13/14 jika ada
                                            // End Menampilakn Nilai Gaji 13/14 jika ada
                                        ?>

                                    <?php
                                        $total=array($row['realisasi_januari'],$row['realisasi_februari'],$row['realisasi_maret'],
                                                $row['realisasi_april'],$row['realisasi_mei'],$row['realisasi_juni'],
                                                $row['realisasi_juli'],$row['realisasi_agustus'],$row['realisasi_september'],
                                                $row['realisasi_oktober'],$row['realisasi_november'],$row['realisasi_desember']);
                                    ?>
                                            <!-- Jumlah Realisasi -->
                                            <!-- Jumlah Realisasi -->
                                            <td align="right"><?php echo number_format($jumlahrealisasi); ?></td>
                                            <!-- Prediksi -->
                                            <!-- Prediksi -->
                                            <?php $prediksi = $row[NamaRealisasi($_POST['bulan'])] * (14-$last_kolom) ?>
                                            <td align="right"><?php echo number_format($prediksi); ?></td>
                                            <!-- Kenaikan Gaji -->
                                            <!-- Kenaikan Gaji -->
                                            <?php $kenaikan_gaji = $row[NamaRealisasi($_POST['bulan'])] * 5 / 100 * 14 ?>
                                            <td align="right"><?php echo number_format($kenaikan_gaji); ?></td>
                                            <!-- Total -->
                                            <!-- Total -->
                                            <?php $total = $jumlahrealisasi+$prediksi+$kenaikan_gaji ?>
                                            <td align="right"><?php echo number_format($total); ?></td>
                                            <!-- Selisih -->
                                            <!-- Selisih -->
                                            <?php
                                                $selisih = $row['anggaran'] - $total;
                                                if ($selisih < 0) {
                                            ?>
                                                <td align="right"><mark style="background-color: #FFFF00;">(<?php echo number_format(abs($selisih)); ?>)</mark></td>
                                            <?php
                                                } else {
                                            ?>
                                                <td align="right"><?php echo number_format($selisih); ?></td>
                                            <?php
                                                }
                                            ?>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    </tbody>
                                </table>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->