<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8">
  	<title>BTL</title>
  	<!-- jQuery and Plugin References -->
  	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  	<script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.18.js"></script>
	
	<script type="text/javascript">
		$(function($) {
	      	$('#txtNumber').autoNumeric('init', {  lZero: 'deny', aSep: ',', mDec: 0 });    
	    });  

		$(function($) {
			$('form').submit(function(){
			    var form = $(this);
			    $('input').each(function(i){
			        var self = $(this);
			        try{
			            var v = self.autoNumeric('get');
			            self.autoNumeric('destroy');
			            self.val(v);
			        }catch(err){
			            console.log("Not an autonumeric field: " + self.attr("name"));
			        }
			    });
			    return true;
			});
	    });
	</script>
</head>
<body>

	<form action="" method="post" enctype="multipart/form-data">
  		<input type="text" id="txtNumber" name="angka" >
  		<button type="submit" class="btn btn-primary" name="edit">
            <i class="fa fa-check"></i> Cek
        </button>
	</form>

	<?php 
		if(isset($_POST['edit']))
		{
			echo ">>".$_POST['angka']."<<";
		}
	?>
</body>
</html>