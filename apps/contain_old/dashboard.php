<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <?php include "../include/connect.php"; ?>
    <?php 
        $data_user = $db->prepare("SELECT MAX(id_pengguna) AS user FROM pengguna");
        $data_user -> execute();
        $max_user = $data_user -> fetch(PDO::FETCH_ASSOC);
        $user = $max_user['user'];

        $data_wp = $db->prepare("SELECT MAX(id_restoran) AS wp FROM restoran");
        $data_wp -> execute();
        $max_wp = $data_wp -> fetch(PDO::FETCH_ASSOC);
        $wp = $max_wp['wp'];

        $data_laporan_perorangan = $db->prepare("SELECT MAX(id_entry_restoran) AS laporan_orang FROM entry_restoran");
        $data_laporan_perorangan -> execute();
        $max_laporan = $data_laporan_perorangan -> fetch(PDO::FETCH_ASSOC);
        $laporan_orang = $max_laporan['laporan_orang'];
    ?>
    <!--
    <div class="col-6 col-lg-3">
        <a href="index.php?contain=master_pengguna">
        <div class="card no-padding bg-flat-color-1">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="fa fa-users text-light"></i>
                </div>

                <div class="h4 mb-0 text-light">
                    <span class=""><?php echo $user; ?></span>
                </div>

                <small class="text-uppercase font-weight-bold text-light">User</small>
                <div class="progress progress-xs mt-3 mb-0 text-light" style="width: 40%; height: 5px;"></div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-6 col-lg-3">
        <a href="index.php?contain=master_wp">
        <div class="card no-padding bg-flat-color-5">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="fa fa-users text-light"></i>
                </div>

                <div class="h4 mb-0 text-light">
                    <span class=""><?php echo $wp; ?></span>
                </div>

                <small class="text-uppercase font-weight-bold text-light">Wajib Pajak</small>
                <div class="progress progress-xs mt-3 mb-0 text-light" style="width: 40%; height: 5px;"></div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-6 col-lg-3">
        <a href="index.php?contain=laporan_perorangan">
        <div class="card no-padding bg-flat-color-2">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="fa fa-users text-light"></i>
                </div>

                <div class="h4 mb-0 text-light">
                    <span class=""><?php echo $laporan_orang; ?></span>
                </div>

                <small class="text-uppercase font-weight-bold text-light">Laporan Perorangan</small>
                <div class="progress progress-xs mt-3 mb-0 text-light" style="width: 40%; height: 5px;"></div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-6 col-lg-3">
        <a href="">
        <div class="card no-padding bg-flat-color-4">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="fa fa-users text-light"></i>
                </div>

                <div class="h4 mb-0 text-light">
                    <span class="">0</span>
                </div>

                <small class="text-uppercase font-weight-bold text-light">Laporan Bulanan Ini</small>
                <div class="progress progress-xs mt-3 mb-0 text-light" style="width: 40%; height: 5px;"></div>
            </div>
        </div>
        </a>
    </div>
    -->
    <!--/.col-->

</div> <!-- .content -->